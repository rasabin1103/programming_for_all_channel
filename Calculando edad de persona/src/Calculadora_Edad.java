import java.text.ParseException;

import javax.swing.JOptionPane;

public class Calculadora_Edad {

	public static void main(String[] args) throws ParseException {
		
		persona persona = new persona();
		
		String fechaNacimiento = JOptionPane.showInputDialog("Inserte su fecha de nacimiento");
		String horaNacimiento = JOptionPane.showInputDialog("Inserte su hora de nacimiento (horas:minutos:segundos)");
		
		persona.setFechaNacimiento(fechaNacimiento);
		persona.setHoraNacimiento(horaNacimiento);
		
		persona.calculateAge();
	}

}
