import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;

public class persona {

	// Atributos de la clase
	String fechaNacimiento;
	String horaNacimiento;

	// Máscaras para usar en los tiempos que capturamos
	public static SimpleDateFormat sdfResult = new SimpleDateFormat("HH:mm:ss");
	public static SimpleDateFormat sdfResultHoras = new SimpleDateFormat("h");
	public static SimpleDateFormat sdfResultMinutos = new SimpleDateFormat("m");
	public static SimpleDateFormat sdfResultSegundos = new SimpleDateFormat("s");

	public persona() {
	}

	public persona(String fechaNacimiento, String horaNacimiento) {
		super();
		this.fechaNacimiento = fechaNacimiento;
		this.horaNacimiento = horaNacimiento;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getHoraNacimiento() {
		return horaNacimiento;
	}

	public void setHoraNacimiento(String horaNacimiento) {
		this.horaNacimiento = horaNacimiento;
	}

	public void calculateAge() throws ParseException {

		// Creamos una máscara para la fecha de nacimiento
		DateTimeFormatter mascaraFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		// Cogemos el atributo de fecha de nacimiento y le damos el formato que deseamos
		LocalDate fechaNac = LocalDate.parse(getFechaNacimiento(), mascaraFecha);
		// Obtenemos la fecha actual del PC
		LocalDate fechaActual = LocalDate.now();
		// Usamos el objeto Period de Java para sacar la diferencia entre dos fechas
		// Con esto tenemos los año, meses y dias de la persona en análisis
		Period periodo = Period.between(fechaNac, fechaActual);

		// Comenzamos con las horas, minutos y segundos

		// Creamos una variable con el tiempo local actual
		LocalTime tiempoLocal = LocalTime.now();

		// Guardamos en una variable Date la diferencia de los tiempos del usuario y el
		// actual usando la máscara configurada al inicio
		Date difference = calcularDiferenciaTiempo(sdfResult.parse(getHoraNacimiento()),
				sdfResult.parse(tiempoLocal.toString()));
		// Mostrando con un JOptionPane el resultado del análisis de la edad global de
		// una persona
		JOptionPane.showMessageDialog(null,
				"Su edad es : " + periodo.getYears() + " Años " + periodo.getMonths() + " Meses " + periodo.getDays()
						+ " Dias " + sdfResultHoras.format(difference) + " Horas " + sdfResultMinutos.format(difference)
						+ " Minutos " + sdfResultSegundos.format(difference) + " Segundos");
	}

	public static Date calcularDiferenciaTiempo(Date dateInicio, Date dateFinal) {

		// Guardamos en una variable long la diferencia de milisegundos entre ambas
		// fechas.
		// Con el getTime() se obtienen los milisegundos de ambas fechas
		long milliseconds = dateFinal.getTime() - dateInicio.getTime();
		// Guardamos en variables enteras los resultados para horas, minutos y segundos
		// multiplicando respectivamente por los valores correspondientes.
		int seconds = (int) (milliseconds / 1000) % 60;
		int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
		int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

		// Creamos una variable calendario para ponerle estos valores y retornar esto
		// como un solo valor de tiempo.
		Calendar c = Calendar.getInstance();

		c.set(Calendar.SECOND, seconds);
		c.set(Calendar.MINUTE, minutes);
		c.set(Calendar.HOUR_OF_DAY, hours);
		return c.getTime();
	}
}
