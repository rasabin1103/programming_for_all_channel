package Problema2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solucion1 {

	public static void main(String[] args) {
		        
		        // Solicitamos por consola el primero de los n�meros enteros
				System.out.println("Inserte primer n�mero entero: ");

				// Guardamos en una variable de tipo BufferedReader lo que se ha escrito por
				// consulta
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

				// Leyendo datos usando readLine
				String number1 = null;

				try {
					number1 = reader.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// Solicitamos por consola el segundo de los n�meros enteros
				System.out.println("Inserte segundo n�mero entero: ");

				// Leyendo datos usando readLine
				String number2 = null;

				try {
					number2 = reader.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(Integer.parseInt(number1) > Integer.parseInt(number2))
					
				{
					System.out.println("Soluci�n : " + Integer.parseInt(number1) + " es mayor que " + Integer.parseInt(number2));	
				}
				
				if(Integer.parseInt(number1) < Integer.parseInt(number2))
					
				{
					System.out.println("Soluci�n : " +Integer.parseInt(number1) + " es menor que " + Integer.parseInt(number2));	
				}
				
				if(Integer.parseInt(number1) == Integer.parseInt(number2))
					
				{
					System.out.println("Soluci�n : " +Integer.parseInt(number1) + " es igual a " + Integer.parseInt(number2));	
				} 


	}

}
