package Problema7;

import java.text.ParseException;

/*
 * 
 * IMC = peso [kg]/ estatura [m]^2
 * 
 * */


import javax.swing.JOptionPane;

public class imc {

	public static void main(String[] args) throws ParseException {

		String estatura = JOptionPane.showInputDialog("Inserte su estatura en m (1.88)");
		String peso = JOptionPane.showInputDialog("Inserte su peso en KG");

		double estaturaNumber = Float.parseFloat(estatura);
		double pesoNumber = Float.parseFloat(peso);
	
		double imc = pesoNumber / Math.pow(estaturaNumber, 2);

		if (imc < 18.5) {
			JOptionPane.showMessageDialog(null,
					"Su indice de masa corporal es : " + imc + " que se encuentra en el rango - Inferior al Normal",
					"Soluci�n", JOptionPane.INFORMATION_MESSAGE);
		}

		if (imc >= 18.5 && imc <= 24.9) {
			JOptionPane.showMessageDialog(null,
					"Su indice de masa corporal es : " + imc + " que se encuentra en el rango - Normal", "Soluci�n",
					JOptionPane.INFORMATION_MESSAGE);
		}

		if (imc >= 25.0 && imc <= 29.9) {
			JOptionPane.showMessageDialog(null,
					"Su indice de masa corporal es : " + imc + " que se encuentra en el rango - Superior al Normal",
					"Soluci�n", JOptionPane.INFORMATION_MESSAGE);
		}

		if (imc > 30.5) {
			JOptionPane.showMessageDialog(null,
					"Su indice de masa corporal es : " + imc + " que se encuentra en el rango - Obesidad", "Soluci�n",
					JOptionPane.INFORMATION_MESSAGE);
		}

	}

}
