package Problema4;

import java.text.ParseException;

import javax.swing.JOptionPane;

public class Solution2 {

	public static void main(String[] args) throws ParseException {

		String number1 = JOptionPane.showInputDialog("Inserte primer n�mero entero");
		String number2 = JOptionPane.showInputDialog("Inserte segundo n�mero entero");

		Object[] possibleValues = { "Seleccione una operaci�n", "Suma", "Resta", "Divisi�n", "Multiplicaci�n" };

		Object selectedValue = JOptionPane.showInputDialog(null, "Choose one", "Input", JOptionPane.INFORMATION_MESSAGE,
				null, possibleValues, possibleValues[0]);

		
		
		// Creamos una instancia de nuestro objeto de operations

		Operations operations = new Operations();

		operations.setNumber1(Integer.parseInt(number1));

		operations.setNumber2(Integer.parseInt(number2));

		// Imprimimos los resultados

		// instrucci�n switch con tipo de datos int
		switch (selectedValue.toString()) {
		case "Suma":
			JOptionPane.showMessageDialog(null, "La suma de sus n�meros es: " + operations.Suma(), "Soluci�n",
					JOptionPane.INFORMATION_MESSAGE);
			break;
		case "Resta":
			JOptionPane.showMessageDialog(null, "La resta de sus n�meros es: " + operations.Resta(), "Soluci�n",
					JOptionPane.INFORMATION_MESSAGE);

			break;
		case "Divisi�n":
			JOptionPane.showMessageDialog(null, "La divisi�n de sus n�meros es: " + operations.Division(), "Soluci�n",
					JOptionPane.INFORMATION_MESSAGE);
			break;
		case "Multiplicaci�n":
			JOptionPane.showMessageDialog(null, "La multiplicaci�n de sus n�meros es: " + operations.Multiplicacion(),
					"Soluci�n", JOptionPane.INFORMATION_MESSAGE);

			break;

		case "Seleccione una operaci�n":
			JOptionPane.showMessageDialog(null, "La selecci�n realizada no es v�lida", "Error",
					JOptionPane.ERROR_MESSAGE);

			break;

		}

	}
}
