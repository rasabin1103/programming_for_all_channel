package Problema6;

import java.text.ParseException;

import javax.swing.JOptionPane;

/*
 * La funci�n factorial se representa con un signo de exclamaci�n �!� detr�s de un n�mero. 
 * Esta exclamaci�n quiere decir que hay que multiplicar todos los n�meros enteros positivos que hay entre ese n�mero y el 1
 * 
 * Ejemplo 5! = 5*4*3*2*1= 120
 * 
 * */

public class factorial {

	public static void main(String[] args) throws ParseException {

		String value = JOptionPane.showInputDialog("Inserte n�mero para c�lculo de factorial");

		int number = Integer.parseInt(value);

		int result = 1;

		while (number != 0) {
			result = result * number;
			number--;
		}

		JOptionPane.showMessageDialog(null, "El factorial de: " + value + " es : " + result, "Soluci�n",
				JOptionPane.INFORMATION_MESSAGE);

	}
}
