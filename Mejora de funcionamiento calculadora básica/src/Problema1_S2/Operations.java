package Problema1_S2;

public class Operations {

	// Atributos de la clase o objeto de operaciones
	int number1;
	int number2;

	// Constructor por defectos donde no usamos par�metros, creando objeto vacio
	public Operations() {
	}

	// Constructor por defectos donde usamos par�metros, creando objeto con valores
	public Operations(int number1, int number2) {
		super();
		this.number1 = number1;
		this.number2 = number2;
	}

	// M�todos que muestra el comportamiento del objeto y lo que se puede realizar
	// con �l
	public int getNumber1() {
		return number1;
	}

	public void setNumber1(int number1) {
		this.number1 = number1;
	}

	public int getNumber2() {
		return number2;
	}

	public void setNumber2(int number2) {
		this.number2 = number2;
	}

	// M�todos para las operaciones solicitadas

	public int Suma() {
		return number1 + number2;
	}

	public int Resta() {
		return number1 - number2;
	}

	public int Multiplicacion() {
		return number1 * number2;
	}

	public int Division() {
		return number1 / number2;
	}

}
