package Problema1_S1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class Solution1 {

	public static void main(String[] args) throws ParseException {

		// Soluci�n sin aplicar la programaci�n orientada a objetos

		// Solicitamos por consola el primero de los n�meros enteros
		System.out.println("Inserte primer n�mero entero: ");

		// Guardamos en una variable de tipo BufferedReader lo que se ha escrito por
		// consulta
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		// Leyendo datos usando readLine
		String number1 = null;

		try {
			number1 = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Solicitamos por consola el segundo de los n�meros enteros
		System.out.println("Inserte segundo n�mero entero: ");

		// Leyendo datos usando readLine
		String number2 = null;

		try {
			number2 = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Realizamos las operaciones solicitadas, usando la clase Integer para
		// convertir los string leidos en numeros
		int suma = Integer.parseInt(number1) + Integer.parseInt(number2);
		int resta = Integer.parseInt(number1) - Integer.parseInt(number2);
		int multiplicacion = Integer.parseInt(number1) * Integer.parseInt(number2);
		int division = Integer.parseInt(number1) / Integer.parseInt(number2);

		// Imprimimos los resultados
		System.out.println("La suma de sus n�meros es: " + suma);
		System.out.println("La resta de sus n�meros es: " + resta);
		System.out.println("La multiplicaci�n de sus n�meros es: " + multiplicacion);
		System.out.println("La divisi�n de sus n�meros es: " + division);

	}

}
