package Problema5;

import java.text.ParseException;

/*
 * �rea = PI * R^2
 * */

import javax.swing.JOptionPane;

public class AreaCircunferencia {

	public static void main(String[] args) throws ParseException {

		String radio = JOptionPane.showInputDialog("Inserte el radio de la circunferencia");
		
        double area = Math.PI * Math.pow(Integer.parseInt(radio),2);
        
        double areaRedondeada = Math.round(area);

		
			JOptionPane.showMessageDialog(null, "El �rea de la circunferencia con radio: "+ Integer.parseInt(radio)+" es : " + area + " Redondeada: "+ areaRedondeada, "Soluci�n",
					JOptionPane.INFORMATION_MESSAGE);
				

	}
}
