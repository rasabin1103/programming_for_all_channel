package Problema1_S2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class Solution2 {

	public static void main(String[] args) throws ParseException {

		// Soluci�n aplicando la programaci�n orientada a objetos

		// Solicitamos por consola el primero de los n�meros enteros
		System.out.println("Inserte primer n�mero entero: ");

		// Guardamos en una variable de tipo BufferedReader lo que se ha escrito por
		// consulta
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		// Leyendo datos usando readLine
		String number1 = null;

		try {
			number1 = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Solicitamos por consola el segundo de los n�meros enteros
		System.out.println("Inserte segundo n�mero entero: ");

		// Leyendo datos usando readLine
		String number2 = null;

		try {
			number2 = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Creamos una instancia de nuestro objeto de operations

		Operations operations = new Operations();
		
		operations.setNumber1(Integer.parseInt(number1));
		
		operations.setNumber2(Integer.parseInt(number2));

		// Usamos este objeto para realizar las operaciones que necesitamos

		/*int suma = operations.Suma();
		int resta = operations.Resta();
		int multiplicacion = operations.Multiplicacion();
		int division = operations.Division();
		*/

		// Imprimimos los resultados
		System.out.println("La suma de sus n�meros es: " + operations.Suma());
		System.out.println("La resta de sus n�meros es: " + operations.Resta());
		System.out.println("La multiplicaci�n de sus n�meros es: " + operations.Multiplicacion());
		System.out.println("La divisi�n de sus n�meros es: " + operations.Division());

	}

}
