package Problema8;

import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class pares {

	public static void main(String[] args) throws ParseException {

		String rangosuperior = JOptionPane.showInputDialog("Inserte rango superior");
		
		ArrayList<Integer> numPar = new ArrayList<Integer>();
		
		int sumPar = 0;
		
		String numerosPares = "";

		int rangosuperiorNumber = Integer.parseInt(rangosuperior);

		for (int i = 1; i < rangosuperiorNumber; i++) {

			if (i % 2 == 0) {
				numPar.add(i);
				numerosPares = numerosPares + " " + String.valueOf(i);
				sumPar = sumPar + i;
			}

		}

		JOptionPane
				.showMessageDialog(null,
						"Los n�meros pares en el rango de (0-" + rangosuperiorNumber + ") son : " + numerosPares
								+ " y la suma de estos n�meros es: " + sumPar,
						"Soluci�n", JOptionPane.INFORMATION_MESSAGE);

	}

}
